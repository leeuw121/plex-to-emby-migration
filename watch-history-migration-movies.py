from plexapi.myplex import MyPlexAccount

import os
import json
import argparse
import requests
import logging
import datetime
import sys
from urllib.parse import urlencode
from requests import Session
from requests.adapters import HTTPAdapter
from requests.exceptions import RequestException

# Configure the logger
logging.basicConfig(filename='logs/watch-history-movies.log', filemode='w', format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logging.debug(f'{datetime.datetime.now()} - This message will get logged into a file')

# Create a custom logger for standard output
stdout_logger = logging.getLogger('stdout_logger')
stdout_handler = logging.StreamHandler(sys.stdout)
stdout_logger.addHandler(stdout_handler)
stdout_logger.setLevel(logging.INFO)

# Configure the stdout_handler to write to the log file
stdout_handler.setFormatter(logging.Formatter('%(asctime)s - %(message)s'))
stdout_handler.setLevel(logging.INFO)
stdout_handler.setStream(open('watch-history-movies.log', 'a'))

# Disable the default stdout_logger.info function
def stdout_logger_info(*args, **kwargs):
    sep = kwargs.get('sep', ' ')
    end = kwargs.get('end', '\n')
    file = kwargs.get('file', sys.stdout)
    output = sep.join(map(str, args)) + end
    file.write(output)

VERIFY_SSL = True

PLEX_SERVER_NAME = ""
PLEX_MOVIE_LIBRARIES = ""
EMBY_URL = ""
EMBY_APIKEY = ""

## The plex token should be for the user you are trying to migrate history for.
# Check out the readme for more info
PLEX_ADMIN_USER_TOKEN = ""
EMBY_ADMIN_USERNAME = ""

# If plex home user doesnt have a pin just leave it blank. If you don't have any home users just leave it set as: HOME_USER_MAPPINGS = []
# Example: HOME_USER_MAPPINGS = [
#          {"plexUsername": "test1", "plexPin": "1111", "embyUsername": "test1"},
#          {"plexUsername": "test2", "plexPin": "", "embyUsername": "test2"}
#]
HOME_USER_MAPPINGS = []



# ----------------- [Do not edit below this line] ----------------- 
class emby:
    def __init__(self, url, apikey, verify_ssl=False, debug=None):
        self.url = url
        self.apikey = apikey
        self.debug = debug

        self.session = Session()
        self.adapters = HTTPAdapter(max_retries=3,
                                    pool_connections=1,
                                    pool_maxsize=1,
                                    pool_block=True)
        self.session.mount('http://', self.adapters)
        self.session.mount('https://', self.adapters)

        # Ignore verifying the SSL certificate
        if verify_ssl is False:
            self.session.verify = False
            # Disable the warning that the request is insecure, we know that...
            import urllib3
            urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    def _call_api(self, cmd, method='GET', payload=None, data=None):
        params = (
            ('api_key', self.apikey),
        )

        if payload:
            params = params + payload

        headers = {
            'accept': '',
            'Content-Type': 'application/json',
        }

        if method == 'GET':
            try:
                response = requests.get(self.url + cmd, headers=headers, params=params, data=data)
            except RequestException as e:
                stdout_logger.info("EMBY request failed for cmd '{}'. Invalid EMBY URL? Error: {}".format(cmd, e))
        elif method == 'POST':
            try:
                response = requests.post(self.url + cmd, headers=headers, params=params, data=data)
            except RequestException as e:
                stdout_logger.info("EMBY request failed for cmd '{}'. Invalid EMBY URL? Error: {}".format(cmd, e))
        if response.status_code != 204:
            try:
                response_json = json.loads(response.text)
            except ValueError:
                stdout_logger.info(
                    "Failed to parse json response for Emby API cmd '{}': {}"
                    .format(cmd, response.content))
                return
        if response.status_code == 200:
            if self.debug:
                stdout_logger.info("Successfully called Emby API cmd '{}'".format(cmd))
            return response_json
        elif response.status_code == 204:
            if self.debug:
                stdout_logger.info("Successfully called Emby API cmd '{}'".format(cmd))
        else:
            error_msg = response.reason
            stdout_logger.info("Emby API cmd '{}' failed: {}".format(cmd, error_msg))
            return

    def get_emby_movies(self, userID=None):
        path = f"/Users/{userID}/Items?"
        params = {"Recursive": "true", "IncludeItemTypes": "Movie", "IsPlayed": "False", 'Fields': 'ProviderIds'}
        cmd = path + urlencode(params)
        return self._call_api(cmd, 'GET')
    def get_emby_movie_by_provider_id(self, providerId=None):
        path = f"/Items?"
        params = {"Recursive": "true",'AnyProviderIdEquals': providerId, 'Fields': 'ProviderIds', 'IncludeItemTypes': 'Movie'}
        cmd = path + urlencode(params)
        return self._call_api(cmd, 'GET')
    def set_emby_played(self, userID=None, videoID=None):
        payload = ()
        data = "{\'Played\':\'True\'}"
        path = f"/Users/{userID}/PlayedItems/{videoID}"
        return self._call_api(path, 'POST', payload, data)
    def GetEmbyIDFromUsername(self, username):
        path = f"/Users"
        params = {}
        cmd = path + urlencode(params)
        users = self._call_api(cmd, 'GET')
        for u in users:
            if u['Name'] == username:
                return u['Id']
        return None
    
def migrateMovieHistory(plexClient, embyUserName):
    stdout_logger.info(f"\nStarting Movie Matching for user: {embyUserName}.")
    embyClient = emby(EMBY_URL.rstrip('/'), EMBY_APIKEY, VERIFY_SSL)
    embyUserId = embyClient.GetEmbyIDFromUsername(embyUserName)
    plexMovieLibraries = PLEX_MOVIE_LIBRARIES.split(",")
    for library in  plexMovieLibraries:
        for plexMovie in plexClient.library.section(library).search(unwatched=False):
                embyWatched = False
                

                for id in plexMovie.guids:
                    provider = id.id.split('://')
                    em = embyClient.get_emby_movie_by_provider_id(f"{provider[0]}.{provider[1]}")
                    
                    
                    if len(em['Items']) > 0 and embyWatched == False:
                        for m in em['Items']:
                            embyClient.set_emby_played(embyUserId, m["Id"])
                            embyWatched = True
                            stdout_logger.info(f"movie match success using {provider[0]} -- {m['Name']}")

                if embyWatched == False:
                    unmatchedMovies.append(f"{embyUserName} - plexMovie.title")

unmatchedMovies = []

adminAccount = MyPlexAccount(token=PLEX_ADMIN_USER_TOKEN)
plexClientAdminUser = adminAccount.resource(PLEX_SERVER_NAME).connect()
migrateMovieHistory(plexClientAdminUser, EMBY_ADMIN_USERNAME)

for user in HOME_USER_MAPPINGS:
    if user['plexPin'] == "":
        account = MyPlexAccount.switchHomeUser(adminAccount, user=user['plexUsername'])
    else:
        account = MyPlexAccount.switchHomeUser(adminAccount, user=user['plexUsername'], pin=user['plexPin'])
    plexClientHomeUser = account.resource(PLEX_SERVER_NAME).connect()
    migrateMovieHistory(plexClientHomeUser, user['embyUsername'])
    migrateShowsHistory(plexClientHomeUser, user['embyUsername'])

if len(unmatchedMovies) > 0:
    stdout_logger.info("\nMatch NOT found for the following Movies:")
    for i in unmatchedMovies:
        stdout_logger.info(i)
