Edit from the below:
I've split both migrations to be one for movies and one for tv.
For myself, I have multiple TV libraries (Cartoons, TV, Anime etc) so to lighten the load on the script, i've split these apart so it's not constantly trying to migrate Movies every single time.
With this, the aditions made to these are:

- Watchdata Scripts will run and output into "logs/[scriptname].log"
- The initial coding has changed with small keyword changes to fit the changes in API's etc.

Learning from this I would ensure the below:

- Ensure the Plex libraries are using either TVDB or TMDB instead of the Plex Series metadata, if you have to change this, I would highly recommend refreshing metadata. (Regarding the Hetzner difficulties, moving the plex server's full database into a localhosted service will assist in doing so)
- Gaining the shared users' plex tokens has been difficult to do so without asking them to acquire this token for you. (I am looking into doing this via Plex User ID and just relying on the "Claimed Server Admin Token")
- Some episodes in shows still have issues with matching. This can be a case where some words in episode names still aren't 100% accurate. EG: "Those Who Hunt: START" on Emby, and "Those Who Hunt: Start" on Plex. These won't migrate over until you change this on either Emby or Plex, however I would recommend changing this on Plex.

# Plex To Emby Migration
This project contains a few python scripts that will help you migrate some of the data from your plex server to you emby server. The scripts match using IMDB, TVDB , and TMDB.  Right now there are two main scripts.

The first one is named metadata-migration.py . It will migrate the following data:

- Posters
- Title
- Original Title
- Sort Title
- Parental Rating
- Labels/Tags
 

The second one is named watched-history-migration.py . This script can migrate watched history for any user but there is a caveat. You have to be able to get that user's plex token. Checkout the readme in the project repository for more details. It will migrate the following data:

- Watched History

There are a few things that you will to be aware of. Due to how the plex api works you can't pull in the watched history of users that you share your server with using the admin plex api key. If you want to migrate multiple users you will need to run this script separetly for each user. You will need to get the plex api token for each user by using this guide:

https://support.plex.tv/articles/204059436-finding-an-authentication-token-x-plex-token/

One other think to note is the script doesn't always find matches for specials. It tries to match IMDB/TVDB/TMDB IDs.

You will need to update the following values at the top of each script before you try and run them.

```
PLEX_URL = "http://localhost:32400/"
PLEX_TOKEN = ""
PLEX_SERVER_NAME = "SomeFriendlyName"
PLEX_MOVIE_LIBRARIES = "Movies"
PLEX_SERIES_LIBRARIES = "TV Shows"
EMBY_URL = "http://localhost:8096/emby/"
EMBY_APIKEY = ""
EMBY_USERNAME = ""
```
