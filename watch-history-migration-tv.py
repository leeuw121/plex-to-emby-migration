from plexapi.myplex import MyPlexAccount

import os
import json
import argparse
import requests
import logging
import datetime
import sys
from urllib.parse import urlencode
from requests import Session
from requests.adapters import HTTPAdapter
from requests.exceptions import RequestException

# Configure the logger
logging.basicConfig(filename='logs/watch-history-tv.log', filemode='w', format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logging.debug(f'{datetime.datetime.now()} - This message will get logged into a file')

# Create a custom logger for standard output
stdout_logger = logging.getLogger('stdout_logger')
stdout_handler = logging.StreamHandler(sys.stdout)
stdout_logger.addHandler(stdout_handler)
stdout_logger.setLevel(logging.INFO)

# Configure the stdout_handler to write to the log file
stdout_handler.setFormatter(logging.Formatter('%(asctime)s - %(message)s'))
stdout_handler.setLevel(logging.INFO)
stdout_handler.setStream(open('watch-history-movies.log', 'a'))

# Disable the default stdout_logger.info function
def stdout_logger_info(*args, **kwargs):
    sep = kwargs.get('sep', ' ')
    end = kwargs.get('end', '\n')
    file = kwargs.get('file', sys.stdout)
    output = sep.join(map(str, args)) + end
    file.write(output)

VERIFY_SSL = True

PLEX_SERVER_NAME = ""
PLEX_SERIES_LIBRARIES = ""
EMBY_URL = ""
EMBY_APIKEY = ""

## The plex token should be for the user you are trying to migrate history for.
# Check out the readme for more info
PLEX_ADMIN_USER_TOKEN = ""
EMBY_ADMIN_USERNAME = ""


# If plex home user doesnt have a pin just leave it blank. If you don't have any home users just leave it set as: HOME_USER_MAPPINGS = []
# Example: HOME_USER_MAPPINGS = [
#          {"plexUsername": "test1", "plexPin": "1111", "embyUsername": "test1"},
#          {"plexUsername": "test2", "plexPin": "", "embyUsername": "test2"}
#]

HOME_USER_MAPPINGS = []



# ----------------- [Do not edit below this line] ----------------- 
class emby:
    def __init__(self, url, apikey, verify_ssl=False, debug=None):
        self.url = url
        self.apikey = apikey
        self.debug = debug

        self.session = Session()
        self.adapters = HTTPAdapter(max_retries=3,
                                    pool_connections=1,
                                    pool_maxsize=1,
                                    pool_block=True)
        self.session.mount('http://', self.adapters)
        self.session.mount('https://', self.adapters)

        # Ignore verifying the SSL certificate
        if verify_ssl is False:
            self.session.verify = False
            # Disable the warning that the request is insecure, we know that...
            import urllib3
            urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    def _call_api(self, cmd, method='GET', payload=None, data=None):
        params = (
            ('api_key', self.apikey),
        )

        if payload:
            params = params + payload

        headers = {
            'accept': '',
            'Content-Type': 'application/json',
        }

        if method == 'GET':
            try:
                response = requests.get(self.url + cmd, headers=headers, params=params, data=data)
            except RequestException as e:
                stdout_logger.info("EMBY request failed for cmd '{}'. Invalid EMBY URL? Error: {}".format(cmd, e))
        elif method == 'POST':
            try:
                response = requests.post(self.url + cmd, headers=headers, params=params, data=data)
            except RequestException as e:
                stdout_logger.info("EMBY request failed for cmd '{}'. Invalid EMBY URL? Error: {}".format(cmd, e))
        if response.status_code != 204:
            try:
                response_json = json.loads(response.text)
            except ValueError:
                stdout_logger.info(
                    "Failed to parse json response for Emby API cmd '{}': {}"
                    .format(cmd, response.content))
                return
        if response.status_code == 200:
            if self.debug:
                stdout_logger.info("Successfully called Emby API cmd '{}'".format(cmd))
            return response_json
        elif response.status_code == 204:
            if self.debug:
                stdout_logger.info("Successfully called Emby API cmd '{}'".format(cmd))
        else:
            error_msg = response.reason
            stdout_logger.info("Emby API cmd '{}' failed: {}".format(cmd, error_msg))
            return

    def get_emby_shows(self, userID=None):
        path = f"/Users/{userID}/Items?"
        params = {"Recursive": "true", "IncludeItemTypes": "Series", "IsPlayed": "False", 'Fields': 'ProviderIds'}
        cmd = path + urlencode(params)
        return self._call_api(cmd, 'GET')
    def get_emby_show_by_name(self, userID=None, showName=None):
        path = f"/Users/{userID}/Items?"
        params = {"Recursive": "true", "SearchTerm": showName, "IncludeItemTypes": "Series", 'Fields': 'ProviderIds'}
        cmd = path + urlencode(params)
        return self._call_api(cmd, 'GET')
    def get_emby_series_by_provider_id(self, providerId=None):
        path = f"/Items?"
        params = {"Recursive": "true",'AnyProviderIdEquals': providerId, 'Fields': 'ProviderIds'}
        cmd = path + urlencode(params)
        return self._call_api(cmd, 'GET')
    def get_emby_episodes(self, userID=None, parentID=None):
        path = f"/Users/{userID}/Items?"
        params = {"Recursive": "true", "ParentId": parentID, "IncludeItemTypes": "Episode", "IsPlayed": "False", 'Fields': 'ProviderIds'}
        cmd = path + urlencode(params)
        return self._call_api(cmd, 'GET')
    def set_emby_played(self, userID=None, videoID=None):
        payload = {}
        data = json.dumps({'Played': True})  # Set the 'Played' field to True
        path = f"/Users/{userID}/PlayedItems/{videoID}"
        return self._call_api(path, 'POST', payload, data)
    def GetEmbyIDFromUsername(self, username):
        path = f"/Users"
        params = {}
        cmd = path + urlencode(params)
        users = self._call_api(cmd, 'GET')
        for u in users:
            if u['Name'] == username:
                return u['Id']
        return None

from concurrent.futures import ThreadPoolExecutor

MAX_WORKERS = 10  # Adjust the number of parallel workers according to your system's capacity

def process_batch_of_episodes(plex_episodes, embyClient, embyUserId):
    for pe in plex_episodes:
        if pe.isPlayed:
            embyWatched = False
            for id in pe.guids:
                provider = id.id.split('://')
                embyEpisodes = embyClient.get_emby_series_by_provider_id(f'{provider[0]}.{provider[1]}')

                for ee in embyEpisodes['Items']:
                    if 'Name' in ee and ee['Name'] == pe.title:
                        embyClient.set_emby_played(embyUserId, ee["Id"])
                        embyWatched = True
                        stdout_logger.info(f"{datetime.datetime.now()} - Episode match success using {provider[0]} -- {plexShow.title} - {pe.seasonEpisode} - {pe.title}")
                        break

            if not embyWatched:
                unmatchedSeries.append(f"{embyUserName} - {plexShow.title} - {pe.seasonEpisode} - {pe.title}")

def migrateShowsHistory(plexClient, embyUserName):
    stdout_logger.info(f"{datetime.datetime.now()} - \nStarting TV Series Matching for user: {embyUserName}.")
    embyClient = emby(EMBY_URL.rstrip('/'), EMBY_APIKEY, VERIFY_SSL)
    embyUserId = embyClient.GetEmbyIDFromUsername(embyUserName)
    plexSeriesLibraries = PLEX_SERIES_LIBRARIES.split(",")
    unmatchedSeries = []  # Initialize unmatchedSeries list here

    for library in plexSeriesLibraries:
        for plexShow in plexClient.library.section(library).search(unwatched=False):
            plexSeasons = plexShow.seasons()
            plexEpisodes = plexShow.episodes()

            for pe in plexEpisodes:
                if pe.isPlayed == True:
                    stdout_logger.info(f"Plex Episode: {pe.title} - {pe.seasonEpisode}")
                    embyWatched = False
                    for id in pe.guids:
                        provider = id.id.split('://')
                        stdout_logger.info(f"Plex Provider ID: {id.id}")
                        embyEpisodes = embyClient.get_emby_series_by_provider_id(f'{provider[0]}.{provider[1]}')
                        stdout_logger.info(f"Emby Episodes: {embyEpisodes}")

                        stdout_logger.info(f"Plex Episode: {pe.title} - {pe.seasonEpisode}")
                        stdout_logger.info(f"Emby Episodes: {embyEpisodes}")

                        for ee in embyEpisodes['Items']:
                            if 'Name' in ee and ee['Name'] == pe.title:
                                embyClient.set_emby_played(embyUserId, ee["Id"])
                                embyWatched = True
                                stdout_logger.info(f"Episode match success using {provider[0]} -- {plexShow.title} - {pe.seasonEpisode} - {pe.title}")
                                break

                    if not embyWatched:  # Handle unmatched episodes here
                        unmatchedSeries.append(f"{embyUserName} - {plexShow.title} - {pe.seasonEpisode} - {pe.title}")

    return unmatchedSeries  # Return the unmatchedSeries list

adminAccount = MyPlexAccount(token=PLEX_ADMIN_USER_TOKEN)
plexClientAdminUser = adminAccount.resource(PLEX_SERVER_NAME).connect()

unmatchedSeries = migrateShowsHistory(plexClientAdminUser, EMBY_ADMIN_USERNAME)

for user in HOME_USER_MAPPINGS:
    if user['plexPin'] == "":
        account = MyPlexAccount.switchHomeUser(adminAccount, user=user['plexUsername'])
    else:
        account = MyPlexAccount.switchHomeUser(adminAccount, user=user['plexUsername'], pin=user['plexPin'])
    plexClientHomeUser = account.resource(PLEX_SERVER_NAME).connect()
    migrateMovieHistory(plexClientHomeUser, user['embyUsername'])
    migrateShowsHistory(plexClientHomeUser, user['embyUsername'])

if len(unmatchedSeries) > 0:
    stdout_logger.info(f"{datetime.datetime.now()} - \nMatch NOT found for the following Series:")
    for i in unmatchedSeries:
        stdout_logger.info(i)
